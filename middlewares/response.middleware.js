const responseMiddleware = (req, res) => {
  // TODO: Implement middleware that returns result of the query
  // if (req.ok)
  //   Также необходимо реализовать middleware для выдачи ответа сервера по следующим правилам:

  //! Ошибки запроса (валидация, проблемы в обработке) - вернуть статус 400 и JSON с ошибкой
  //! Если что-то не найдено - вернуть статус 404 и JSON с ошибкой JSON ошибки формата
  // Постарайтесь давать лаконичные, но понятные сообщения об ошибках, например:
  //* User not found
  //* User entity to create isn't valid
  if (req.data) {
    res.status(200).json(req.data);
  } else if (req.err) {
    res.status(404).json({ error: true, message: req.err.message });
  } else {
    res.status(400).json({ error: true, message: "Trouble with server" });
  }
};

exports.responseMiddleware = responseMiddleware;

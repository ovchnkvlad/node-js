const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { name, power, defense } = req.body;
  if (req.body.id) {
    res.status(400).json({
      error: true,
      message: "id in req",
    });
  }
  if (name === null || name === "" || name === undefined) {
    res.status(400).json({
      error: true,
      message: "Fighter name field isn't create",
    });
  } else if (power === null || power === "" || power === undefined) {
    res.status(400).json({
      error: true,
      message: "Fighter power field isn't create",
    });
  } else if (isNaN(power)) {
    res.status(400).json({
      error: true,
      message: "The power should be a number",
    });
  } else if (isNaN(defense)) {
    res.status(400).json({
      error: true,
      message: "The defense should be a number",
    });
  } else if (power < 1 || power > 100 || power === 0) {
    res.status(400).json({
      error: true,
      message: "The power should be 0 < power < 100",
    });
  } else if (defense < 0 || defense > 10) {
    res.status(400).json({
      error: true,
      message: "The power should be 0 < power < 100",
    });
  } else {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  const { name, power, defense } = req.body;
  if (req.body.id) {
    res.status(400).json({
      error: true,
      message: "id in req",
    });
  }
  if (defense < 0 || defense > 10) {
    res.status(400).json({
      error: true,
      message: "The power should be 0 < power < 100",
    });
  } else if (isNaN(power)) {
    res.status(400).json({
      error: true,
      message: "The power should be a number",
    });
  } else if (isNaN(defense)) {
    res.status(400).json({
      error: true,
      message: "The defense should be a number",
    });
  } else if (power < 0 || power > 100 || power === 0) {
    res.status(400).json({
      error: true,
      message: "The power should be 0 < power < 100",
    });
  } else if (defense < 1 || defense > 10) {
    res.status(400).json({
      error: true,
      message: "The power should be 0 < power < 100",
    });
  } else {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

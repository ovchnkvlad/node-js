const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  createFighter(data) {
    const name = { name: data.name };

    const uniqueName = FighterRepository.getOne(name);
    let user;
    if (uniqueName) {
      user = { error: true, message: "The name of fighter isn't unique" };
    } else {
      user = FighterRepository.create(data);
    }
    return user;
  }
  getAllFighter() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      throw Error("No figthers created");
    }
    return fighters;
  }
  getFighterById(id) {
    const item = FighterRepository.getOne(id);
    if (!item) {
      return null;
    }
    return item;
  }
  deleteFighterById(id) {
    const item = FighterRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();

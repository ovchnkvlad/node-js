const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();
//! POST /api/users
router.post(
  "/",
  createUserValid,
  (req, res, next) => {
    try {
      const user = UserService.createUser(req.body);
      if (user) {
        req.data = user;
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
//! PUT /api/users/:id
router.put(
  "/:id",
  updateUserValid,
  (req, res, next) => {
    try {
      const user = UserService.updateUser(req.params, req.body);
      console.log(usern);
      if (user) {
        req.data = user;
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
//! GET /api/users
router.get(
  "/",
  (req, res, next) => {
    try {
      const user = UserService.getAllUser();
      if (user) {
        req.data = user;
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
//! GET /api/users/:id
router.get(
  "/:id",
  (req, res, next) => {
    try {
      const user = UserService.getUserById(req.params);
      if (user) {
        req.data = user;
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
//! DELETE /api/users/:id
router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const user = UserService.deleteUserById(req.params);
      if (user) {
        res.status(200).json({ message: "User was delete succsessful" });
      }
    } catch (err) {
      req.err = err;
    }
  },
  responseMiddleware
);

module.exports = router;

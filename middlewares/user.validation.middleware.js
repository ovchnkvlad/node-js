const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  // Наличие полей
  // Формат полей:
  // email - только gmail почты
  // phoneNumber: +380xxxxxxxxx
  // power - число и < 100
  // Id в body запросов должен отсутствовать
  // Лишние поля не должны пройти в БД
  // const regExpForGmail = new RegExp("^w+@gmail.[a-zA-Z]{2,3}$$");
  const regExpForGmail = new RegExp(
    "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@g(oogle)?mail.com$"
  );
  const regExpForPhone = new RegExp("[^+?3?8?(0d{9})$]");

  const { firstName, lastName, email, phoneNumber, password } = req.body;

  delete user.id;
  const userModel = Object.keys(user);
  const bodyModel = Object.keys(req.body);

  const compareFields = JSON.stringify(userModel) === JSON.stringify(bodyModel);
  if (req.body.id) {
    res.status(400).json({
      error: true,
      message: "Don't use id field in req",
    });
  }
  if (firstName === null || firstName === " " || firstName === undefined) {
    res.status(400).json({
      error: true,
      message: "First name field isn't create",
    });
  } else if (lastName === null || lastName === " " || lastName === undefined) {
    res.status(400).json({
      error: true,
      message: "Last name field isn't create",
    });
  } else if (email === null || email === " " || email === undefined) {
    res.status(400).json({
      error: true,
      message: "Email field isn't create",
    });
  } else if (
    phoneNumber === null ||
    phoneNumber === "" ||
    phoneNumber === undefined
  ) {
    res.status(400).json({
      error: true,
      message: "Phone number field isn't create",
    });
  } else if (password === null || password === " " || password === undefined) {
    res.status(400).json({
      error: true,
      message: "Password field isn't create",
    });
  } else if (!regExpForGmail.test(email)) {
    res.status(400).json({
      error: true,
      message: "Only google mail valid",
    });
  } else if (!regExpForPhone.test(phoneNumber)) {
    res.status(400).json({
      error: true,
      message: "Phone number is invalid",
    });
  } else if (!compareFields) {
    res.status(400).json({
      error: true,
      message: "The invalid input field",
    });
  } else {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const regExpForGmail = new RegExp(
    "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@g(oogle)?mail.com$"
  );
  const regExpForPhone = new RegExp("[^+?3?8?(0d{9})$]");

  const { email, phoneNumber } = req.body;
  delete user.id;
  const userModel = Object.keys(user);
  const bodyModel = Object.keys(req.body);

  const compareFields = JSON.stringify(userModel) === JSON.stringify(bodyModel);

  if (req.body.id) {
    res.status(400).json({
      error: true,
      message: "Don't use id field in req",
    });
  }
  if (email !== undefined && !regExpForGmail.test(email)) {
    res.status(400).json({
      error: true,
      message: "Only google mail valid",
    });
  } else if (email !== undefined && !regExpForPhone.test(phoneNumber)) {
    res.status(400).json({
      error: true,
      message: "Phone number is invalid",
    });
  } else {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

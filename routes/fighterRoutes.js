const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();
//! POST /api/fighters
router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    try {
      const fighter = FighterService.createFighter(req.body);
      req.data = fighter;
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
//! GET /api/fighters
router.get(
  "/",
  (req, res, next) => {
    try {
      const fighter = FighterService.getAllFighter();
      req.data = fighter;
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

//! GET /api/fighters/:id
router.get(
  "/:id",
  (req, res, next) => {
    try {
      const fighter = FighterService.getFighterById(req.params);
      req.data = fighter;
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

//! PUT /api/fighters/:id
router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    try {
      const user = FighterService.updateUser(req.params, req.body);
      if (user) {
        req.data = user;
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);
//! DELETE /api/fighters/:id
router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const user = FighterService.deleteFighterById(req.params);
      if (user) {
        req.data = { message: "Fighter was delete succsessful" };
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;

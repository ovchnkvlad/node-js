const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  createUser(data) {
    const user = UserRepository.create(data);
    return user;
  }
  createUser(data) {
    const email = { email: data.email };
    const phone = { phoneNumber: data.phoneNumber };

    const uniqueMail = UserRepository.getOne(email);
    const uniquePhone = UserRepository.getOne(phone);
    let user;
    if (uniqueMail) {
      throw Error("The email isn't unique");
    } else if (uniquePhone) {
      throw Error("The phone number isn't unique");
    } else {
      return UserRepository.create(data);
    }
  }
  getAllUser() {
    return UserRepository.getAll();
  }
  getUserById(id) {
    const item = UserRepository.getOne(id);
    if (!item) {
      return null;
    }
    return item;
  }
  deleteUserById(id) {
    const item = UserRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }
  updateUser(id, dataToUpdate) {
    const findUser = UserRepository.getOne(id);
    if (!findUser) {
      throw Error("No user created yet");
    }
    const item = UserRepository.update(id, dataToUpdate);

    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();

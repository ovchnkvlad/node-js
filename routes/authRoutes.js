const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      const user = AuthService.login(req.body);
      if (user) {
        req.data = user;
      }
    } catch (err) {
      req.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
